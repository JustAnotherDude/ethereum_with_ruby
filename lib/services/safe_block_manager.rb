class SafeBlockManager
  def self.call(contract)
    redis = Redis.new(url: Rails.configuration.x.redis.connection_string)

    from_block = redis.get(contract.address).to_i || 0
    to_block = Ethereum::Singleton.instance.eth_block_number['result'].to_i(16)

    return if to_block < from_block

    redis.set(contract.address, to_block + 1)
    { from_block: '0x' + from_block.to_s(16), to_block: '0x' + to_block.to_s(16) }
  end
end
